---
title:  "EVAL-AD2S1210SDZ"
image:  "EVAL-AD2S1210SDZ"
ref: "resource"
categories: "resources.md"
id: 3
---

The EVAL-AD2S1210SDZ is a full featured evaluation board designed to allow the
user to easily evaluate all the features of the AD2S1210 resolver-to-digital
converter (RDC). The SDP board (EVAL-SDP-CB1Z) allows the evaluation board to
be controlled through the USB port of a PC using the evaluation board software
available for download from the AD2S1210 product page.

### URLs

* https://www.analog.com/en/design-center/evaluation-hardware-and-software/evaluation-boards-kits/EVAL-AD2S1210.html
