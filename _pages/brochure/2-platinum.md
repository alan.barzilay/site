---
title:  "Platinum"
image:  "platinum"
ref: "platinum"
categories: "brochure.md"
id: 2
---

# Values: R$1500

# Benefits

* Identification of the sponsoring brand in all event material: online and offline.
* Logo on the badge.
* Acknowledgment of the sponsor in all activities related to the event.
* Opportunity to display the sponsor's banner at the event;
* Opportunity to make a 15 minutes speech at the event opening.
